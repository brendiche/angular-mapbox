import { PocMapPage } from './app.po';

describe('poc-map App', () => {
  let page: PocMapPage;

  beforeEach(() => {
    page = new PocMapPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
