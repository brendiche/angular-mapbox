import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { MapBoxModule } from './map-box/module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MapBoxModule.forRoot('pk.eyJ1IjoiYnJlbmRpY2hlIiwiYSI6ImNqNG80ZzA3bTFtM3oyd283bmxicnRtb24ifQ.gl-rG6rqdzmG20JJZ89eJw')
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
