import { Component, EventEmitter } from '@angular/core';
import { MapboxService } from './map-box/services/mapbox.service';
import { mapEvent } from './map-box/mapbox/enums';
import { Layer } from './map-box/class/class';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  map : any;
  test: EventEmitter<{
    instruction : mapEvent,
    data:any
  }>

  config = {
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        center: [1.3967422443185942,43.581353942590454],
        zoom: 11,
        pitch: 0,
        bearing: 0,
      }

  source = {
          'id':'test',
          'type': 'geojson',
          'data': {
              'type': 'Feature',
              'geometry': {
                  'type': 'Polygon',
                  'coordinates': [[
                    [1.40625,43.61572265625],
                    [1.42822265625,43.61572265625],
                    [1.42822265625,43.59375],
                    [1.40625,43.59375]
                  ]]
              }
          }
      }
  _handleClick:any;
  constructor(private mb : MapboxService){
    this.test = new EventEmitter<{
      instruction : mapEvent,
      data:any
    }>();
    this._handleClick = this.click.bind(this);
  }
  public getMap(map){
    this.map = map;
    // this.mb.addNavigationControl(map,'top-right');
    // this.map.on('click',this.click);
    this.test.emit({
      instruction:'addListenerOnce',
      data:{
        event:'click',
        listener : this._handleClick
      }
    });
  }

  private click(event){
    console.log('test',event);
    // this.test.emit({
    //   instruction:'removeListener',
    //   data:{
    //     event:'click',
    //     listener : this._handleClick
    //   }
    // });
  }
  public removeLayer(){
      this.test.emit({
        instruction:'removeLayer',
        data:{
          id:'toto'
        }
      })
  }

  public drawLayer(){
    let data = new Layer({
      id:'toto',
      type:'fill',
      source : 'test',
      'paint': {
        'fill-color': '#088',
        'fill-opacity': 0.8
      }
    })
    this.test.emit({
      instruction:'addLayer',
      data:data
    })
  }

  public removeControl(){
    this.test.emit({
      instruction : 'removeSource',
      data:{
        id : "test"
      }
    })
    // this.test.emit({
    //   instruction : 'removeScaleControl',
    //   data:{}
    // })
  }

  public addLayer(){
    this.test.emit({
      instruction : 'addSource',
      data: this.source
    });
    // this.test.emit({
    //   instruction : 'addScaleControl',
    //   data:{
    //     position : 'top-right'
    //   }
    // });
    // this.map.addLayer({
    //     'id': 'maine',
    //     'type': 'fill',
    //     'source': {
    //         'type': 'geojson',
    //         'data': {
    //             'type': 'Feature',
    //             'geometry': {
    //                 'type': 'Polygon',
    //                 'coordinates': [[
    //                   [1.40625,43.61572265625],
    //                   [1.42822265625,43.61572265625],
    //                   [1.42822265625,43.59375],
    //                   [1.40625,43.59375]
    //                 ]]
    //             }
    //         }
    //     },
    //     'layout': {},
    //     'paint': {
    //         'fill-color': '#088',
    //         'fill-opacity': 0.8
    //     }
    // });
  }

  public addMarker(){
    let element = document.createElement('img');
    element.src = 'http://imap.klickitatcounty.org/img/marker-icon-blue.png';
    let data = {
      id: 'test',
      element : element,
      options : {
        offset:[-12, -41]
      },
      lng:1.42822265625,
      lat:43.61572265625,
    }
    this.test.emit({
      instruction:'addMarker',
      data:data
    });
  }

  public removeMarker(){
    this.test.emit({
      instruction:'removeMarker',
      data:{
        id:'test'
      }
    })
  }

  public getZoom(){
    console.log(this.map.getZoom());
  }
}
