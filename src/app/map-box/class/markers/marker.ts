import * as mapboxgl from 'mapbox-gl';
export  class Marker {
  private _id:string;
  private _lat:number;
  private _lng:number;
  private _popup:any;
  private _element:HTMLElement;
  private _options:{
    offset:[number,number]
  }

  private _marker:mapboxgl.Marker;

  constructor(marker:{
    id?:string
    lat?:number,
    lng?:number,
    popup?:any,
    element:HTMLElement,
    options?:{
      offset:[number,number]
    }
  }){
    this._id = marker.id ? marker.id : 'marker'+Math.random().toString()+new Date().getTime().toString();
    this._lat = marker.lat;
    this._lng = marker.lng;
    this._popup = marker.popup;
    this._element = marker.element;
    this._options = marker.options;
    if (this._options) {
      this._marker = new mapboxgl.Marker(this._element,this._options);
    }else{
      this._marker = new mapboxgl.Marker(this._element);
    }
    if(marker.lat && marker.lng){
      this._marker.setLngLat([this._lng,this._lat]);
    }
  }

  public addTo(map: mapboxgl.Map,){
    this._marker.addTo(map);
  }

  public remove(){
    this._marker.remove();
  }

  /**
   * GETTER & SETTER
   */

  get id():string{
    return this._id;
  }

  get LngLat():{lng:number,lat:number}{
    return {
      lng:this._lng,
      lat:this._lat
    };
  }

  set LngLat(lngLat:{lng:number,lat:number}){
    this._lat = lngLat.lat;
    this._lng = lngLat.lng;
    this._marker.setLngLat({lng:this._lng,lat:this._lat});
  }

  get element():HTMLElement{
    return this._marker.getElement();
  }
}
