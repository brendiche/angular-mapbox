import { Type } from './enums';
export class Layer {
  private _id:string;
  private _type:Type;
  private _metadata:any;
  private _ref:string;
  private _source:string;
  private _sourceLayer:string;
  private _minzoom:number;
  private _maxzoom:number;
  private _filter:any;
  private _layout:any;
  private _paint:any;

	constructor(layer : {
                        id:string,
                        type?:Type,
                        metadata?:any,
                        ref?:string,
                        source?:string,
                        sourceLayer?:string,
                        minzoom?:number,
                        maxzoom?:number,
                        filter?:any,
                        layout?:any,
                        paint?:any
                      }){
    this._id = layer.id;
    this._type = layer.type;
    this._metadata = layer.metadata;
    this._ref = layer.ref;
    this._source = layer.source;
    this._sourceLayer = layer.sourceLayer;
    this._minzoom = layer.minzoom;
    this._maxzoom = layer.maxzoom;
    this._filter = layer.filter;
    this._layout = layer.layout;
    this._paint = layer.paint;
	}

  public toJSON(){
    let layer = {
      'id' : this.id
    };
    if(this._type){
      layer['type']= this._type;
    }
    if(this._metadata){
      layer['metadata']= this._metadata;
    }
    if(this._ref){
      layer['ref']= this._ref;
    }
    if(this._source){
      layer['source']= this._source;
    }
    if(this._sourceLayer){
      layer['source-layer']= this._sourceLayer;
    }
    if(this._minzoom){
      layer['minzoom']= this._minzoom;
    }
    if(this._maxzoom){
      layer['maxzoom']= this._maxzoom;
    }
    if(this._filter){
      layer['filter']= this._filter;
    }
    if(this._layout){
      layer['layout']= this._layout;
    }
    if(this._paint){
      layer['paint']= this._paint;
    }
    return layer
  }

  /**
   * GETTER & SETTER
   */
  get id(){
    return this._id;
  }

  get source(){
    return this._source;
  }

}
