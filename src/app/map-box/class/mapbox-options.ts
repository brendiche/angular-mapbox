export class MapboxOptions {
	private _container
	private _minZoom
	private _maxZoom
	private _style
	private _hash
	private _interactive
	private _bearingSnap
	private _pitchWithRotate
	private _classes
	private _attributionControl
	private _logoPosition
	private _failIfMajorPerformanceCaveat
	private _preserveDrawingBuffer
	private _refreshExpiredTiles
	private _maxBounds
	private _scrollZoom
	private _boxZoom
	private _dragRotate
	private _dragPan
	private _keyboard
	private _doubleClickZoom
	private _touchZoomRotate
	private _trackResize
	private _center
	private _zoom
	private _pitch
	private _renderWorldCopies
	constructor(){

	}
}
