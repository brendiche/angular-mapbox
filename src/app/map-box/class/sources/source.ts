import {sourceType} from './enums';
export class Source {
  private _id:string;
  private _type:sourceType;
  private _data:any;

  constructor(source : {
    id?:string,
    type:sourceType
    data:any
  }){
    this._type = source.type;
    this._data = source.data;
    this._id = source.id ? source.id : 'source'+Math.random().toString()+new Date().getTime().toString()
  }

  public toJson():{type:sourceType,data:any}{
    return {
      type:this._type,
      data:this._data
    }
  }

  public addFeature(geometry:{type:string,coordinates:Array<number>}){
    if(this._type === 'geojson' && this._data.type && this._data.type === 'FeatureCollection'){
      this._data.features.push({
        type : 'Feature',
        geometry : geometry
      })
    }
  }

  /**
   * GETTER & SETTER
   */
   get id():string{
     return this._id
   }

   set data(data:any){
     this._data = data;
   }
}
