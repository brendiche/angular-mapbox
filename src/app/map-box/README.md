# Angular module wrapper for mapbox


This module is create from angular cli

## Requierment
[Mapbox](https://www.mapbox.com/mapbox-gl-js/api/)

```shell
npm install --save mapbox-gl
```

## Installation

```shell
npm install --save angular-mapbox
```

## Get Started

<p>Add the module to your main module app</p>

```javascript
//app.module.ts
...
import { MapBoxModule } from 'angular-mapbox/module';
...
@NgModule({
    ...
    imports: [
        ...
        MapBoxModule.forRoot('YOU_MAPBOX_API_KEY')
        ...
    ],
    ...
});
```

## Usage
<p>You can add the balise in your html</p>

```html
<ng-mapbox></ng-mapbox>
```

#### Parameters :

- input
  - config : the map box configuration
  - instructions : the event emiter to give instruction to the map

- output
  - map : gave an observable return the map instance
  - informations : gave an observable to get information whene a layer or marker is added to the map


## Events

events                  | descriptions                       | data parameter
------------------------|------------------------------------|------------------------------------------------
addControl              | add a mapbox control to the map    | {control : mapboxgl.Control, position : string}
removeControl           | remove a mapbox control to the map | control : mapboxgl.Control
addNavigationControl    | add Navigation control             | position : string
removeNavigationControl | remove Navigation control          | none
addScaleControl         | add Scale control                  | position : string
removeScaleControl      | remove Navigation control          | none
addSource               | add Source to the map              | data is Source type
removeSource            | remove Source to the map           | id : string
addLayer                | add Layer to the map               | data is Layer type
removeLayer             | remove Layer to the map            | id : string
addMarker               | add Marker to the map              | data is Marker type
removeMarke             | remove Marker to the map           | id : string
