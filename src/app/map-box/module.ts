import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapboxService } from './services/mapbox.service';
import { MapboxComponent } from './mapbox/mapbox.component'
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MapboxComponent],
  exports:[
  	MapboxComponent
  ]
})
export class MapBoxModule {
	static forRoot(config: String): ModuleWithProviders {
        return {
            ngModule: MapBoxModule,
            providers: [
                { provide: 'MAPBOX_KEY', useValue: config },
                MapboxService
            ]
        };
    }
}
