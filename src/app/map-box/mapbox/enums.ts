type  mapEvent =
  'addControl'|
  'removeControl'|
  'addNavigationControl'|
  'removeNavigationControl'|
  'addScaleControl'|
  'removeScaleControl'|
  'addSource'|
  'removeSource'|
  'addLayer'|
  'addMarker'|
  'removeLayer'|
  'removeMarker'|
  'addListener'|
  'addListenerOnce'|
  'removeListener'

export {mapEvent};
